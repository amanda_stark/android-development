package com.amanda.hellotest;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import java.io.File;
import java.io.IOException;


public class MainActivity extends ActionBarActivity {

    int TAKE_PHOTO_CODE = 1;
    int ACTIVITY_SELECT_IMAGE = 2;
    public static int count = 0;
    String dir = "";
    String currentPhotoPath = "";
    Button button;
    Button button1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.welcome_screen);

        // Make a folder called picFolder that stores the pictures taken by the camera
        dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/picFolder/";
        File newDir = new File(dir);
        newDir.mkdirs();

        addListenerOnButtons();
    }

    private void addListenerOnButtons() {
        final Context context = this;
        button = (Button) findViewById(R.id.takePicture);

        // Method that should allow the user to take a picture using their phone
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String file = dir + "picture.jpg";
                File newFile = new File(file);
                try {
                    newFile.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                Uri outputFileUri = Uri.fromFile(newFile);

                /*
                currentPhotoPath = "file:" + newFile.getAbsolutePath();
                // Adds the image to the photo library
                Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                File f = new File(currentPhotoPath);
                mediaScanIntent.setData(outputFileUri);
                sendBroadcast(mediaScanIntent);
                */
                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);

                startActivityForResult(cameraIntent, TAKE_PHOTO_CODE);
            }
        });

        button1 = (Button) findViewById(R.id.choosePicture);

        // method that should allow the user to choose a picture from their photo library
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                count++; // increment counter to append image name that will be saved
                String file = dir + count + ".jpg";
                File newFile = new File(file);
                try {
                    newFile.createNewFile();
                } catch (IOException e) {}

                Uri outputFileUri = Uri.fromFile(newFile);

                Intent intent = new Intent(Intent.ACTION_PICK,MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(intent, ACTIVITY_SELECT_IMAGE);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == TAKE_PHOTO_CODE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            Bitmap imageBitMap = (Bitmap) extras.get("data");
            //ImageView.setImageBitmap(imageBitmap);
            Log.d("CameraDemo", "Picture saved using camera");
        } // if
        else if(requestCode == ACTIVITY_SELECT_IMAGE && resultCode == RESULT_OK) {
            Log.d("CameraDemo", "Picture saved using photo library");
        }
        else {
            Log.d("CameraDemo", "Picture could not be taken");
        } // else
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
