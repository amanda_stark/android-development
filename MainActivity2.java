package com.amanda.hellotest;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.Image;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;

/**
 * Created by Amanda on 3/1/2015.
 */
public class MainActivity2 extends Activity {

    Button button1;
    Button button2;
    ImageView image;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ask_to_perform);
        displayImage();
        addListenersOnButtons();
    }

    private void displayImage() {
        image = (ImageView) findViewById(R.id.imageView);
    }

    private void addListenersOnButtons() {
        final Context context = this;

        button1 = (Button) findViewById(R.id.riskAssessment);
        button2 = (Button) findViewById(R.id.goBack);

    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            Bitmap photo = (Bitmap) data.getExtras().get("data");
            image.setImageBitmap(photo);
        }
    }
}
